#!/usr/bin/env bash

if [ ! -f /data/secrets.crt ]; then
    echo "waiting for secrets.crt file"
fi
while [ ! -f /data/secrets.crt ]
do
    sleep 1
done

if [ ! -f /data/secrets.key ]; then
    echo "waiting for secrets.key file"
fi
while [ ! -f /data/secrets.key ]
do
    sleep 1
done

python -m secrets
