FROM python:3.6
MAINTAINER Srinivas Devaki (mr.eightnoteight@gmail.com)

COPY secrets /secrets
COPY run.sh /secrets/run.sh

RUN pip install -r /secrets/requirements.txt

VOLUME /data

EXPOSE 9001

CMD ["bash", "/secrets/run.sh"]
