from .app import app, ssl_context
import aiohttp.web

aiohttp.web.run_app(app, host='0.0.0.0', port=9001, ssl_context=ssl_context)
