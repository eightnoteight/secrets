import ssl
import aiohttp.web
from .routes import setup_routes

HTTPS_SECRETS_CRT_FILE_PATH = '/data/secrets.crt'
HTTPS_SECRETS_KEY_FILE_PATH = '/data/secrets.key'

ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
ssl_context.load_cert_chain(HTTPS_SECRETS_CRT_FILE_PATH, HTTPS_SECRETS_KEY_FILE_PATH)


app = aiohttp.web.Application()
setup_routes(app)
