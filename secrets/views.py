from aiohttp.web import Request, Response
import asyncio
import aiofiles
import aiofiles.os
import re
import os

loop = asyncio.get_event_loop()


def validate_request_key_name(method):
    async def new_method(request: Request):
        key_name = request.match_info['key_name']
        if not re.match(r'^([a-zA-Z0-9_\-]|\.)+$', key_name):
            return Response(status=404)
        return await method(request)
    return new_method


@validate_request_key_name
async def get_secret(request: Request):
    key_name = request.match_info['key_name']
    if not await loop.run_in_executor(None, os.path.isfile, f'/data/key_{key_name}'):
        return Response(status=404)
    async with aiofiles.open(f'/data/key_{key_name}', 'r') as f:
        value = await f.read()
    if value is None:
        return Response(status=404)
    return Response(text=value)


@validate_request_key_name
async def post_secret(request: Request):
    key_name = request.match_info['key_name']
    if await loop.run_in_executor(None, os.path.isfile, f'/data/key_{key_name}'):
        return Response(status=303)
    async with aiofiles.open(f'/data/key_{key_name}', 'wb') as f:
        async for chunk in request.content.iter_chunked(1024):
            await f.write(chunk)
    return Response(text='')


@validate_request_key_name
async def put_secret(request):
    key_name = request.match_info['key_name']
    async with aiofiles.open(f'/data/key_{key_name}', 'wb') as f:
        async for chunk in request.content.iter_chunked(1024):
            await f.write(chunk)
    return Response(text='')


@validate_request_key_name
async def delete_secret(request):
    key_name = request.match_info['key_name']
    if not await loop.run_in_executor(None, os.path.isfile, f'/data/key_{key_name}'):
        return Response(status=404)
    await loop.run_in_executor(None, os.remove, f'/data/key_{key_name}')
    return Response(text='')
