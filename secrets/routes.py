from .views import get_secret, post_secret, put_secret, delete_secret
from aiohttp.web import Application


def setup_routes(app: Application):
    app.router.add_get('/{key_name}', get_secret)
    app.router.add_post('/{key_name}', post_secret)
    app.router.add_put('/{key_name}', put_secret)
    app.router.add_delete('/{key_name}', delete_secret)
